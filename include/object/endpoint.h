/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <types.h>
#include <object/structures.h>

static inline tcb_queue_t PURE ep_ptr_get_queue(endpoint_t *epptr)
{
    tcb_queue_t queue;

    queue.head = (tcb_t *)endpoint_ptr_get_epQueue_head(epptr);
    queue.end = (tcb_t *)endpoint_ptr_get_epQueue_tail(epptr);

    return queue;
}

exception_t sendIPC(bool_t blocking, bool_t do_call, word_t badge,
             bool_t canGrant, bool_t canGrantReply, bool_t canDonate,
             bool_t isLong, bool_t firstPhase, tcb_t *thread,
             endpoint_t *epptr);
exception_t receiveIPC(tcb_t *thread, cap_t cap, bool_t isBlocking,
             bool_t isLong, bool_t isPoll, cap_t replyCPtr);
void reorderEP(endpoint_t *epptr, tcb_t *thread);
bool_t pauseIPC(tcb_t *tptr);
void cancelIPC(tcb_t *tptr);
void softCancelIPC(tcb_t *tptr);
void cancelAllIPC(endpoint_t *epptr);
void cancelBadgedSends(endpoint_t *epptr, word_t badge);
void replyFromKernel_error(tcb_t *thread);
void transferFailed(tcb_t *thread);
void replyFromKernel_success_empty(tcb_t *thread);
void replyFromKernel_success_empty_send(tcb_t *thread);

