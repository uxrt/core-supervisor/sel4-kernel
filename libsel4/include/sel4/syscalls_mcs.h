/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include <autoconf.h>

/**
 * @defgroup MCSSystemCalls MCS System Calls
 * @{
 */

/**
 * @xmlonly <manual name="Send" label="sel4_mcs_send"/> @endxmlonly
 * @brief Send to a capability
 *
 * @xmlonly
 * <docref>See <autoref label="sec:sys_send"/></docref>
 * @endxmlonly
 *
 * @param[in] dest The capability to be invoked.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_Send(seL4_CPtr dest, seL4_MessageInfo_t msgInfo);

/**
 * @xmlonly <manual name="Recv" label="sel4_mcs_recv"/> @endxmlonly
 * @brief Block until a message is received on an endpoint
 *
 * @xmlonly
 * <docref>See <autoref label="sec:sys_recv"/></docref>
 * @endxmlonly
 *
 * Differences from seL4:
 *
 * If a long message was received, the capsUnwrapped field of the tag will be
 * non-zero, but the extraCaps field will be zero; this is otherwise invalid.
 * The first slot of caps_or_badges will contain the size of the long message
 * buffers.
 *
 * If a long message was received, its send buffers may be read with
 * seL4_LongReadBuf (same as for messages received with seL4_LongRecv) and its
 * reply buffers may be written with seL4_LongWriteBuf.
 *
 * @param[in] src The capability to be invoked.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 * @param[in] reply The capability to the reply object to use on a call (only used on MCS).
 *
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_Recv(seL4_CPtr src, seL4_Word *sender, seL4_CPtr reply);

/**
 * @xmlonly <manual name="Call" label="sel4_mcs_call"/> @endxmlonly
 * @brief  Call a capability
 *
 * @xmlonly
 * <docref>See <autoref label="sec:sys_call"/></docref>
 * @endxmlonly
 *
 * @param[in] dest The capability to be invoked.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 *
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_Call(seL4_CPtr dest, seL4_MessageInfo_t msgInfo);


/**
 * @xmlonly <manual name="Non-Blocking Send" label="sel4_mcs_nbsend"/> @endxmlonly
 * @brief Perform a non-blocking send to a capability
 *
 * @xmlonly
 * <docref>See <autoref label="sec:sys_nbsend"/></docref>
 * @endxmlonly
 *
 * @param[in] dest The capability to be invoked.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 */
LIBSEL4_INLINE_FUNC void
seL4_NBSend(seL4_CPtr dest, seL4_MessageInfo_t msgInfo);


/**
 * @xmlonly <manual name="Reply Recv" label="sel4_mcs_replyrecv"/> @endxmlonly
 * @brief Perform a reply followed by a receive in one system call
 *
 * @xmlonly
 * <docref>See <autoref label="sec:sys_replyrecv"/></docref>
 * @endxmlonly
 *
 * Differences from seL4:
 *
 * Long messages are handled in the same way as with seL4_Recv.
 *
 * @param[in] src The capability to perform the receive on.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 *
 * @param[in] reply The capability to the reply object, which is first invoked and then used
 *                  for the recv phase to store a new reply capability.
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_ReplyRecv(seL4_CPtr src, seL4_MessageInfo_t msgInfo, seL4_Word *sender, seL4_CPtr reply);

/**
 * @xmlonly <manual name="NBRecv" label="sel4_mcs_nbrecv"/> @endxmlonly
 * @brief Receive a message from an endpoint but do not block
 *        in the case that no messages are pending
 *
 * @xmlonly
 * <docref>See <autoref label="sec:sys_nbrecv"/></docref>
 * @endxmlonly
 *
 * Differences from seL4:
 *
 * Long messages are handled in the same way as with seL4_Recv.
 *
 * @param[in] src The capability to receive on.
 * @param[out] sender The address to write sender information to.
 *                    The sender information is the badge of the
 *                    endpoint capability that was invoked by the
 *                    sender, or the notification word of the
 *                    notification object that was signalled.
 *                    This parameter is ignored if `NULL`.
 * @param[in] reply The capability to the reply object to use on a call.
 *
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_NBRecv(seL4_CPtr src, seL4_Word *sender, seL4_CPtr reply);

/**
 * @xmlonly <manual name="NBSend Recv" label="sel4_nbsendrecv"/> @endxmlonly
 * @brief Non-blocking send on one capability, and a blocking recieve on another in a single
 *        system call.
 *
 * Differences from seL4:
 *
 * Long messages are handled in the same way as with seL4_Recv.
 *
 * @xmlonly
 * <docref>See <autoref label="sec:sys_nbsendrecv"/></docref>
 * @endxmlonly
 *
 * @param[in] dest The capability to be invoked.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 * @param[in] src The capability to receive on.
 * @param[in] reply The capability to the reply object, which is first invoked and then used
 *                  for the recv phase to store a new reply capability.
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * as described in <autoref label="sec:messageinfo"/>
 * @endxmlonly
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_NBSendRecv(seL4_CPtr dest, seL4_MessageInfo_t msgInfo, seL4_CPtr src, seL4_Word *sender, seL4_CPtr reply);

/**
 * @xmlonly <manual name="NBSend Wait" label="sel4_nbsendwait"/> @endxmlonly
 * @brief Non-blocking invoke of a capability and wait on another in one system call
 *
 * @xmlonly
 * <docref>See <autoref label="sec:sys_nbsendwait"/></docref>
 * @endxmlonly
 *
 * @param[in] dest The capability to be invoked.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 * @param[in] src The capability to receive on.
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * as described in <autoref label="sec:messageinfo"/>
 * @endxmlonly
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_NBSendWait(seL4_CPtr dest, seL4_MessageInfo_t msgInfo, seL4_CPtr src, seL4_Word *sender);

/**
 * @xmlonly <manual name="Yield" label="sel4_mcs_yield"/> @endxmlonly
 * @brief Yield the remaining timeslice. Periodic threads will not be scheduled again until their
 *        next sporadic replenishment.
 *
 * @xmlonly
 * <docref>See <autoref label="sec:sys_yield"/></docref>
 * @endxmlonly
 */
LIBSEL4_INLINE_FUNC void
seL4_Yield(void);

/**
 * @xmlonly <manual name="Wait" label="sel4_mcs_wait"/> @endxmlonly
 * @brief Perform a wait on an endpoint or notification object
 *
 * Block on a notification or endpoint waiting for a message. No reply object is
 * required for a Wait. Wait should not be paired with Call, as it does not provide
 * a reply object. If Wait is paired with a Call the waiter will block after recieving
 * the message.
 *
 * @xmlonly
 * <docref>See the description of <nameref name="seL4_Wait"/> in <autoref label="sec:sys_wait"/>.</docref>
 * @endxmlonly
 *
 * @param[in] src The capability to be invoked.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_Wait(seL4_CPtr src, seL4_Word *sender);

/**
 * @xmlonly <manual name="Wait" label="sel4_mcs_wait"/> @endxmlonly
 * @brief Perform a wait on an endpoint or notification object without actually
 * receiving a message on it
 *
 * Block on a notification or endpoint waiting for a message and return once one
 * is available, but do not transfer the message and leave it available to be
 * received by a regular Recv or Wait call. For notifications, this is
 * equivalent to seL4_Wait.
 *
 * If there are multiple polling threads blocked because no sender was present,
 * once a message is sent, the sender will iterate over the queue unblocking
 * polling threads until either a thread doing a regular receive is encountered
 * (the receive will be processed normally) or no threads are left.
 *
 * @param[in] src The capability to be invoked.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_PollWait(seL4_CPtr src, seL4_Word *sender);

/**
 * @xmlonly <manual name="NBWait" label="sel4_nbwait"/> @endxmlonly
 * @brief Perform a polling wait on an endpoint or notification object
 *
 * Poll a notification or endpoint waiting for a message. No reply object is
 * required for a Wait. Wait should not be paired with Call.
 *
 * @xmlonly
 * <docref>See the description of <nameref name="seL4_NBWait"/> in <autoref label="sec:sys_nbwait"/>.</docref>
 * @endxmlonly
 *
 * @param[in] src The capability to be invoked.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_NBWait(seL4_CPtr src, seL4_Word *sender);

/**
 * @xmlonly <manual name="Poll" label="sel4_mcs_poll"/> @endxmlonly
 * @brief Perform a non-blocking recv on a notification object
 *
 * This is not a proper system call known by the kernel. Rather, it is a
 * convenience wrapper which calls seL4_NBWait().
 * It is useful for doing a non-blocking wait on a notification.
 *
 * @xmlonly
 * <docref>See the description of <nameref name="seL4_NBWait"/> in <autoref label="sec:sys_nbwait"/>.</docref>
 * @endxmlonly
 *
 * @param[in] src The capability to be invoked.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 *
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_Poll(seL4_CPtr src, seL4_Word *sender);

/**
 * @xmlonly <manual name="Signal" label="sel4_mcs_signal"/> @endxmlonly
 * @brief Signal a notification
 *
 * This is not a proper system call known by the kernel. Rather, it is a
 * convenience wrapper which calls seL4_Send().
 * It is useful for signalling a notification.
 *
 * @xmlonly
 * <docref>See the description of <nameref name="seL4_Send"/> in <autoref label="sec:sys_send"/>.</docref>
 * @endxmlonly
 *
 * @param[in] dest The capability to be invoked.
 */
LIBSEL4_INLINE_FUNC void
seL4_Signal(seL4_CPtr dest);


/**
 * @xmlonly <manual name="Long Send" label="sel4_mcs_send"/> @endxmlonly
 * @brief Send a long message to an endpoint or reply object
 *
 * This is similar to seL4_Send, but in addition to copying from the sender's
 * fixed IPC buffer to that of the receiver, it also copies an array of
 * arbitrary-length external buffers that may be located anywhere within the
 * sender's VSpace.
 *
 * In addition to copying into the receiver's long message buffers at send time
 * with this function, data may also be copied using seL4_LongWriteBuf before
 * sending the message. Passing long buffers to this function is entirely
 * optional; the message size received will be set to the highest offset written
 * by either seL4_LongWriteBuf or this function. If long buffers are passed to
 * this function, they will be copied to the start of the receiver's buffers,
 * overwriting anything potentially written there by seL4_LongWriteBuf.
 *
 * The buffer array consists of seL4_LongMsgBuffer structures.
 *
 * Unlike seL4_Send, this function does not support capability transfers, since
 * the caps_or_badges field is used for the addresses of the buffer arrays.
 *
 * This function fails on any objects other than endpoints and replies.
 *
 * @param[in] dest The capability to be invoked.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 * @param[in] longBufs Pointer to the long buffer array.
 * @param[in] longBufCount Size of the long buffer array.
 *
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 *
 * This is used only to return error status; the seL4_IsTransferError flag is
 * always set in the label on an error (to match with Recv functions) since the
 * errors that can occur are limited to the same ones as for receiving.
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_LongSend(seL4_CPtr dest, seL4_MessageInfo_t msgInfo, const seL4_LongMsgBuffer *longBufs, seL4_Word longBufCount, seL4_Word flags);

/**
 * @xmlonly <manual name="Long Recv" label="sel4_mcs_longrecv"/> @endxmlonly
 * @brief Block until a message is received on an endpoint, transferring a long
 * message into user-provided buffers if necessary
 *
 * This is similar to seL4_Receive, but in addition to copying the fixed IPC
 * buffer to that of the receiver, it also copies into an array of
 * arbitrary-length external buffers that may be located anywhere within the
 * receiver's VSpace. The portion of the message in the fixed IPC buffer is
 * always handled as for a short receive, and is never copied into external
 * buffers.
 *
 * This will completely fill each receive buffer before moving on to the next;
 * a send buffer that is longer than a receive buffer will spill over into
 * subsequent receive buffers, and a receive buffer that is longer than a send
 * buffer will have subsequent send buffers copied into it until it is
 * either completely full or the entire message has been transferred. This only
 * applies within a message and not across messages; a single seL4_LongRecv will
 * never combine the results of multiple calls to seL4_LongSend or equivalent.
 *
 * If the entire message is longer than the sum of the receive buffers, use
 * seL4_LongReadBuf to transfer the remainder of it.
 *
 * Unlike seL4_Recv, this function does not support capability transfers, since
 * the caps_or_badges field is used for the addresses of the buffer arrays.
 *
 * @param[in] src The capability to be invoked.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 * @param[in] reply The capability to the reply object to use on a call (only used on MCS).
 * @param[in] longBufs Pointer to the long buffer array.
 * @param[in] longBufCount Size of the long buffer array.
 * @param[out] recvSize Pointer that is set to the total size of the received
 * 		long buffers (does not include the short header portion of the message in
 * 		the fixed buffer).
 * @param[out] recvTotalSize Pointer that is set to the total size of the sent buffer on the
 * 		sender's side.
 * @param[out] recvReplySize Pointer that is set to the total size of the reply buffer on the
 * 		sender's side.
 *
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 *
 * For errors generated by the kernel, the label is or'ed with
 * seL4_IsTransferError.
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_LongRecv(seL4_CPtr src, seL4_Word *sender, seL4_CPtr reply, seL4_LongMsgBuffer *longBufs, seL4_Word longBufCount, seL4_Word *recvSize, seL4_Word *recvTotalSize, seL4_Word *recvReplySize, seL4_Word flags);

/**
 * @xmlonly <manual name="Long Call" label="sel4_mcs_longcall"/> @endxmlonly
 * @brief  Call a capability while sending and/or receiving a long message
 *
 * This is equivalent to seL4_LongSend followed by seL4_LongRecv with an atomic
 * transition between the two. The sending and/or receiving buffer array may be
 * NULL if sending/receiving long buffers isn't required (although there's not
 * really much of a point to using this function rather than seL4_Call if both
 * are NULL).
 *
 * Unlike seL4_Call, this function does not support capability transfers, since
 * the caps_or_badges field is used for the addresses of the buffer arrays.
 *
 * One major difference from seL4_LongRecv is that there is no way to copy from
 * the sender's buffer, since no reply object is returned, but this function is
 * intended for use with clients, which in UX/RT have no way to deal with
 * messages larger than the received buffer anyway (similarly to QNX and
 * conventional Unix-like OSes).
 *
 * @param[in] dest The capability to be invoked.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 * @param[in] sendLongBufs Pointer to the sending long buffer array. Should be
 *               NULL if not sending any long buffers.
 * @param[in] sendLongBufCount Size of the sending long buffer array. Should be
 *               zero if not sending any long buffers.
 * @param[in] recvLongBufs Pointer to the receiving long buffer array. Should be
 *               NULL if not receiving any long buffers.
 * @param[in] recvLongBufCount Size of the receiving long buffer array. Should
 *               be zero if not receiving any long buffers.
 * @param[out] recvLongMsgSize Set to the total size of the received long
 *               buffers (does not include the short header portion of the
 *               message in the fixed buffer).
 *
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 *
 * The seL4_IsTransferError flag will be set in the label for errors that can
 * also be returned from one-way Send/Recv.
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_LongCall(seL4_CPtr dest, seL4_MessageInfo_t msgInfo, const seL4_LongMsgBuffer *sendLongBufs, seL4_Word sendLongBufCount, seL4_LongMsgBuffer *recvLongBufs, seL4_Word recvLongBufCount, seL4_Word *recvLongMsgSize, seL4_Word flags);

/**
 * @xmlonly <manual name="Non-Blocking Long Send" label="sel4_mcs_nblongsend"/> @endxmlonly
 * @brief Perform a non-blocking long send to an endpoint/reply
 *
 * This function is similar to seL4_LongSend, except that the message is
 * silently dropped if no thread is trying to receive on the destination
 * capability.
 *
 * Unlike seL4_NBSend, this function does not support capability transfers,
 * since the caps_or_badges field is used for the addresses of the buffer
 * arrays.
 *
 * @param[in] dest The capability to be invoked.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 * @param[in] longBufs Pointer to the long buffer array.
 * @param[in] longBufCount Size of the long buffer array.

 */
/*LIBSEL4_INLINE_FUNC void
seL4_LongNBSend(seL4_CPtr dest, seL4_MessageInfo_t msgInfo, const seL4_LongMsgBuffer *longBufs, seL4_Word longBufCount);*/

/**
 * @xmlonly <manual name="Long Reply Recv" label="sel4_mcs_longreplyrecv"/> @endxmlonly
 * @brief Perform a long reply followed by a long receive in one system call
 *
 * Unlike seL4_ReplyRecv, this function does not support capability transfers,
 * since the caps_or_badges field is used for the addresses of the buffer
 * arrays.
 *
 * @param[in] src The capability to perform the receive on.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 *
 * @param[in] reply The capability to the reply object, which is first invoked and then used
 *                  for the recv phase to store a new reply capability.
 * @param[in] replyLongBufs Pointer to the replying long buffer array. Should be
 *               NULL if not sending any long buffers.
 * @param[in] replyLongBufCount Size of the replying long buffer array. Should
 *               be zero if not sending any long buffers.
 * @param[in] recvLongBufs Pointer to the receiving long buffer array. Should be
 *               NULL if not receiving any long buffers.
 * @param[in] recvLongBufCount Size of the receiving long buffer array. Should
 *               be zero if not receiving any long buffers.
 * @param[out] recvSize Pointer that is set to the total size of the received
 * 		long buffers (does not include the short header portion of the message in
 * 		the fixed buffer).
 * @param[out] recvTotalSize Pointer that is set to the total size of the sent buffer on the
 * 		sender's side.
 * @param[out] recvReplySize Pointer that is set to the total size of the reply buffer on the
 * 		sender's side.
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 *
 * The seL4_IsTransferError flag will be set in the label for errors returned by
 * the kernel.
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_LongReplyRecv(seL4_CPtr src, seL4_MessageInfo_t msgInfo, seL4_Word *sender, seL4_CPtr reply, const seL4_LongMsgBuffer *replyLongBufs, seL4_Word replyLongBufCount, seL4_LongMsgBuffer *recvLongBufs, seL4_Word recvLongBufCount, seL4_Word *recvSize, seL4_Word *recvTotalSize, seL4_Word *recvReplySize, seL4_Word flags);

/**
 * @xmlonly <manual name="LongNBRecv" label="sel4_mcs_longnbrecv"/> @endxmlonly
 * @brief Receive a long message from an endpoint but do not block
 *        in the case that no messages are pending
 *
 * Unlike seL4_NBRecv, this function does not support capability transfers,
 * since the caps_or_badges field is used for the addresses of the buffer
 * arrays.
 *
 * @param[in] src The capability to receive on.
 * @param[out] sender The address to write sender information to.
 *                    The sender information is the badge of the
 *                    endpoint capability that was invoked by the
 *                    sender, or the notification word of the
 *                    notification object that was signalled.
 *                    This parameter is ignored if `NULL`.
 * @param[in] reply The capability to the reply object to use on a call.
 * @param[in] longBufs Pointer to the long buffer array.
 * @param[in] longBufCount Size of the long buffer array.
 * @param[in] longMsgSize Total size of the received long buffers (does not
 *               include the short header portion of the message in the fixed buffer).
 *
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 */
/*LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_LongNBRecv(seL4_CPtr src, seL4_Word *sender, seL4_CPtr reply, seL4_LongMsgBuffer *longBufs, seL4_Word longBufCount, seL4_Word *longMsgSize);*/

/**
 * @xmlonly <manual name="Long NBSend Recv" label="sel4_longnbsendrecv"/> @endxmlonly
 * @brief Non-blocking long send on one capability, and a blocking long receive
 *        on another in a single system call.
 *
 * Unlike seL4_NBSendRecv, this function does not support capability transfers,
 * since the caps_or_badges field is used for the addresses of the buffer
 * arrays.
 *
 * Currently this only supports long buffers for the receive phase. The send
 * phase is more intended for signalling notifications (e.g. when converting a
 * thread to passive) rather than sending on endpoints.
 *
 * @param[in] dest The capability to be invoked.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 * @param[in] src The capability to receive on.
 * @param[in] reply The capability to the reply object, which is first invoked and then used
 *                  for the recv phase to store a new reply capability.
 * @param[in] recvLongBufs Pointer to the receiving long buffer array. Should be
 *               NULL if not receiving any long buffers.
 * @param[in] recvLongBufCount Size of the receiving long buffer array. Should
 *               be zero if not receiving any long buffers.
 * @param[out] recvSize Pointer that is set to the total size of the received
 * 		long buffers (does not include the short header portion of the message in
 * 		the fixed buffer).
 * @param[out] recvTotalSize Pointer that is set to the total size of the sent buffer on the
 * 		sender's side.
 * @param[out] recvReplySize Pointer that is set to the total size of the reply buffer on the
 * 		sender's side.
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * as described in <autoref label="sec:messageinfo"/>
 * @endxmlonly
 *
 * The seL4_IsTransferError flag will be set in the label for errors returned by
 * the kernel.
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_LongNBSendRecv(seL4_CPtr dest, seL4_MessageInfo_t msgInfo, seL4_CPtr src, seL4_Word *sender, seL4_CPtr reply, seL4_LongMsgBuffer *recvLongBufs, seL4_Word recvLongBufCount, seL4_Word *recvSize, seL4_Word *recvTotalSize, seL4_Word *recvReplySize, seL4_Word flags);

/**
 * @xmlonly <manual name="Long NBSend Wait" label="sel4_longnbsendwait"/> @endxmlonly
 * @brief Non-blocking invoke of a capability and wait on another in one system call
 *
 * Unlike seL4_NBSendRecv, this function does not support capability transfers,
 * since the caps_or_badges field is used for the addresses of the buffer
 * arrays.
 *
 * @param[in] dest The capability to be invoked.
 * @param[in] msgInfo The messageinfo structure for the IPC.
 * @param[out] sender The address to write sender information to.
 *               The sender information is the badge of the
 *               endpoint capability that was invoked by the
 *               sender, or the notification word of the
 *               notification object that was signalled.
 *               This parameter is ignored if `NULL`.
 * @param[in] src The capability to receive on.
 * @param[in] longBufs Pointer to the long buffer array.
 * @param[in] longBufCount Size of the long buffer array.
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * as described in <autoref label="sec:messageinfo"/>
 * @endxmlonly
 */
/*LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_LongNBSendWait(seL4_CPtr dest, seL4_MessageInfo_t msgInfo, seL4_CPtr src, seL4_Word *sender, const seL4_LongMsgBuffer *longBufs, seL4_Word longBufCount);
*/
/**
 * @xmlonly <manual name="Long Read Buffer" label="sel4_mcs_longrecv"/> @endxmlonly
 * @brief Read from the long send buffers of the underlying message of a reply object into a caller-provided vector of buffers.
 *
 * The reply object must have a pending message; once seL4_Send or equivalent
 * has been called on the reply, any attempts to call this function will fail
 * until the reply object has been used again in another receive. This function
 * may be called repeatedly on the same reply object for the same offset.
 *
 * This function immediately starts copying, returning as soon as the copy is
 * finished, and does not put the thread into a blocked state, although the copy
 * may be preempted if it takes long enough.
 *
 * @param[in] reply The capability to the reply object.
 * @param[in] longBufs Pointer to the long buffer array.
 * @param[in] longBufCount Size of the long buffer array.
 * @param[in] offset Starting offset within the sent message to copy from.
 * @param[out] len Set to the total length copied on completion.
 *
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 *
 * This is used only to return error status; the seL4_IsTransferError flag is
 * always set in the label on an error (to match with Recv functions) since the
 * errors that can occur are limited to the same ones as for receiving.
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_LongReadBuf(seL4_CPtr reply, seL4_LongMsgBuffer *longBufs, seL4_Word longBufCount, seL4_Word offset, seL4_Word *len);

/**
 * @xmlonly <manual name="Long Write Buffer" label="sel4_mcs_send"/> @endxmlonly
 * @brief Write from a caller-provided vector of buffers to the long reply buffers of the underlying message of a reply object.
 *
 * The reply object must have a pending message; once seL4_Send or equivalent
 * has been called on the reply, any attempts to call this function will fail
 * until the reply object has been used again in another receive. This function
 * may be called repeatedly on the same reply object for the same offset.
 *
 * This function immediately starts copying, returning as soon as the copy is
 * finished, and does not put the thread into a blocked state, although the copy
 * may be preempted if it takes long enough.
 *
 * To actually send the reply, seL4_Send or seL4_ReplyRecv must be called on
 * the reply. The size of the message will be set to the highest offset written
 * by any invocation of this function before the message is sent (or the highest
 * offset written at send time if it is higher than any written by this
 * function). If buffers are passed when sending the message, they will be
 * written to the start of the receiver's buffer, overwriting anything written
 * there by previous calls to this function.
 *
 * @param[in] reply The capability to the reply object.
 * @param[in] longBufs Pointer to the long buffer array.
 * @param[in] longBufCount Size of the long buffer array.
 * @param[in] offset Starting offset within the sent message to copy from.
 * @param[out] len Set to the total length copied on completion.
 *
 * @return A `seL4_MessageInfo_t` structure
 * @xmlonly
 * <docref>as described in <autoref label="sec:messageinfo"/></docref>
 * @endxmlonly
 *
 * This is used only to return error status; the seL4_IsTransferError flag is
 * always set in the label on an error (to match with Recv functions) since the
 * errors that can occur are limited to the same ones as for receiving.
 */
LIBSEL4_INLINE_FUNC seL4_MessageInfo_t
seL4_LongWriteBuf(seL4_CPtr reply, const seL4_LongMsgBuffer *longBufs, seL4_Word longBufCount, seL4_Word offset, seL4_Word *len);


/** @} */

